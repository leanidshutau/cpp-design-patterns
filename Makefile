MKDIR_P=mkdir -p
TEMP_DIR=tmp
CC=g++
COMPILE_FLAGS=-g -std=c++17 -c -Wall -Werror

subfolders=factory-method abstract-factory

FACTORY_METHOD_OBJS = $(wildcard factory_method/$(TEMP_DIR)/*.o)
ABSTARCT_FACTORY_OBJS = $(wildcard abstract_factory/$(TEMP_DIR)/*.o)

all: main.o $(subfolders) link clean

main.o: main.cc
	$(MKDIR_P) $(TEMP_DIR)
	$(CC) $(COMPILE_FLAGS) main.cc -o $(TEMP_DIR)/main.o

link: main.o $(subfolders)
	$(MKDIR_P) build
	$(CC) $(TEMP_DIR)/main.o $(FACTORY_METHOD_OBJS) $(ABSTARCT_FACTORY_OBJS) -o build/app

$(subfolders):
	$(MAKE) -C ./$(subst -,_,$@)/

clean:
	rm -rf $(TEMP_DIR) $(foreach dir,$(subfolders),$(subst -,_,$(dir))/$(TEMP_DIR))
