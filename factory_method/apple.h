#ifndef __APPLE_CLASS_H__
#define __APPLE_CLASS_H__

#include "food.h"

namespace FactoryMethod
{

class Apple : public IFood
{
public:
  int getNutrition() const;
};

} // namespace FactoryMethod

#endif // __APPLE_CLASS_H__
