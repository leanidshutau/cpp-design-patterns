#include "apple_eater.h"
#include "apple.h"

FactoryMethod::IFood *FactoryMethod::AppleEater::createFood()
{
  return new FactoryMethod::Apple();
}
