#ifndef __APPLE_EATER_CLASS_H__
#define __APPLE_EATER_CLASS_H__

#include "eater.h"

namespace FactoryMethod
{

class AppleEater : public Eater
{
  IFood *createFood();
};

} // namespace FactoryMethod

#endif // __APPLE_EATER_CLASS_H__
