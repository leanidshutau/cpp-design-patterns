#include "eater.h"

FactoryMethod::Eater::Eater() : energy(0){};
FactoryMethod::Eater::Eater(int energy)
{
  this->energy = energy;
}

void FactoryMethod::Eater::eat(const FactoryMethod::IFood &food)
{
  this->energy += food.getNutrition();
}

int FactoryMethod::Eater::getEnergy()
{
  return this->energy;
}
