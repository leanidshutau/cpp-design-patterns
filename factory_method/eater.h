#ifndef __EATER_CLASS_H__
#define __EATER_CLASS_H__

#include "food.h"

namespace FactoryMethod
{

class Eater
{
  int energy;

public:
  Eater();
  Eater(int);
  virtual ~Eater(){};
  void eat(const FactoryMethod::IFood &);
  int getEnergy();
  virtual IFood *createFood() = 0;
};

} // namespace FactoryMethod

#endif // __EATER_CLASS_H__
