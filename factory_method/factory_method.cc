#include "factory_method.h"
#include "apple_eater.h"
#include "orange_eater.h"

std::vector<int> FactoryMethod::run()
{
  FactoryMethod::AppleEater appleEater;
  FactoryMethod::OrangeEater orangeEater;

  FactoryMethod::Eater *eaters[2] = {
      &appleEater,
      &orangeEater,
  };

  std::vector<int> foodEnergy;

  for (auto eater : eaters)
  {
    IFood *food = eater->createFood();

    int oldEnergy = eater->getEnergy();
    eater->eat(*food);

    foodEnergy.push_back(eater->getEnergy() - oldEnergy);

    delete food;
  }

  return foodEnergy;
}
