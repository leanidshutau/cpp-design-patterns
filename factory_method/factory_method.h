#ifndef __FACTORY_METHOD_H__
#define __FACTORY_METHOD_H__

#include <vector>

namespace FactoryMethod
{

std::vector<int> run();

} // namespace FactoryMethod

#endif //__FACTORY_METHODO_H__
