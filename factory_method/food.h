#ifndef __FOOD_INTERFACE_H__
#define __FOOD_INTERFACE_H__

namespace FactoryMethod
{

class IFood
{
public:
  virtual ~IFood(){};
  virtual int getNutrition() const = 0;
};

} // namespace FactoryMethod

#endif // __FOOD_INTERFACE_H__
