#ifndef __ORANGE_CLASS_H__
#define __ORANGE_CLASS_H__

#include "food.h"

namespace FactoryMethod
{

class Orange : public IFood
{
public:
  int getNutrition() const;
};

} // namespace FactoryMethod

#endif // __ORANGE_CLASS_H__
