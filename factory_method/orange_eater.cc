#include "orange_eater.h"
#include "orange.h"

FactoryMethod::IFood *FactoryMethod::OrangeEater::createFood()
{
  return new FactoryMethod::Orange();
}
