#ifndef __ORANGE_EATER_CLASS_H__
#define __ORANGE_EATER_CLASS_H__

#include "eater.h"

namespace FactoryMethod
{

class OrangeEater : public Eater
{
  IFood *createFood();
};

} // namespace FactoryMethod

#endif // __ORANGE_EATER_CLASS_H__
