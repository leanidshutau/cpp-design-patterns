#include <iostream>
#include "string.h"

#include "factory_method/factory_method.h"

int main(int argn, char **args)
{
  if (argn == 1)
  {
    std::cout << "Pattern name is not provided" << std::endl;
    return 0;
  }

  char *pattern = args[1];

  if (strcmp(pattern, "factory-method") == 0)
  {
    std::cout << "Running Factory Method..." << std::endl;
    for (int elem : FactoryMethod::run())
    {
      std::cout << elem << std::endl;
    }
  }

  return 0;
}
